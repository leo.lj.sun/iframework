<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sun
 * Date: 13-9-10
 * Time: 上午9:50
 * To change this template use File | Settings | File Templates.
 */
namespace i\Core;
//use Exception;
class I
{
    private static $config = array();

    public static function play()
    {
        self::init();
        $route = new Route();
        $controller = Route::getNamespaceController();
        if (class_exists($controller)) {
            $controller = new $controller();
        } else {
            throw new \Exception('Controller :' . Route::getNamespaceController() . ' is not exists.');
        }
        /**
         * 假如要加入feather，这里是注入点，可以用反射来读取方法的注释，也可以用反射来执行方法
         */
        if (is_callable(array($controller, Route::getAction()))) {
            $controller->beforeAction(); //beforeAction

            call_user_func_array(array($controller, Route::getAction()), Route::getParams()); //这里会把url的参数直接传到action，不用再从$_GET取了
            $controller->afterAction();
        } else {
            throw new \Exception('Controller :' . Route::getNamespaceController() . ' is not have ' . Route::getAction() . ' Action.');
        }
    }

    public static function getConfig($key)
    {
        return isset(self::$config[$key]) ? self::$config[$key] : null;
    }

    /**
     * 自动加载函数
     */
    private static function autoLoad($className)
    {
        $filePath = strtr($className, '\\', DS);
        $extensions = self::getConfig('Extensions');
	$file = APP_PATH.DS.'Protected'.DS.$filePath.EXT;
        //应用里面不存在则去框架找
        if(!file_exists($file)){
            $file = FRAMEWORK_PATH.$filePath.EXT;
        }
        //去应用的扩展里面找
        if (!file_exists($file)) {
            $file = APP_PATH.DS.'Protected'.DS.'Extensions'.DS.$filePath.EXT;

        }
        //去框架的扩展找,todo 这里不妥，框架换了名字就不好使了
        if(!file_exists($file)){
            $file = FRAMEWORK_PATH .'i'.DS. 'Extensions' . DS . $filePath . EXT;
        }
	
        if (file_exists($file)) {
            require_once($file);
            return;
        } else {
	    if(!DEBUG){
		echo 'bad request!';die;
	    }
	    else{
		 throw new \Exception($className . ' 加载这个类的时候未找到文件 PATH: '.$file);
	    }	
        }
    }

    /**
     * 初始化框架
     * 读取配置文件，
     * 注册自动加载函数
     */
    private static function init()
    {
        //读取项目的配置文件
        $appConfigFile = APP_PATH.DIRECTORY_SEPARATOR.'Protected'.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'config.php';
        if(file_exists($appConfigFile)){
            $appConfig = require_once $appConfigFile;
            if(empty($appConfig)){
                $appConfig = array();
            }
            self::$config = $appConfig;
        }else{
            throw new \Exception($appConfigFile.' is not exists ');
        }
        //读取系统配置，建议不同项目的统一配置写在这个文件中
        $systemConfig = require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config.php';
        self::$config = array_merge($systemConfig,self::$config);//把系统低配置合并到项目配置，以项目配置为主
        spl_autoload_register('self::autoLoad');
        self::isDebug();
    }

    private static function isDebug(){
	if(DEBUG){
            header("Content-type:text/html;charset=utf-8");
            require dirname(__DIR__).DIRECTORY_SEPARATOR.'php_error.php';
	    \php_error\reportErrors();
        }
    }
}
