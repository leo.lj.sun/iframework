<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sun
 * Date: 13-9-10
 * Time: 上午9:46
 * To change this template use File | Settings | File Templates.
 */
namespace i\Core;
final class Route
{

    private static $controller = null;

    private static $action = null;

    private static $urlType = 'pathInfo';

    private static $namespaceController = null;

    /**
     * 获取带命名空间的Controller
     */
    public static function getNamespaceController()
    {
        return (null === self::$namespaceController)
            ? self::$namespaceController =  'Controllers\\' . Route::getController()
            : self::$namespaceController;
    }

    /**
     * 解析url获取Controller名称
     * @return null|string
     */
    public static function getController()
    {
        //静态变量如果存在就不再解析一次，节省资源
        if (null !== self::$controller) {
            return self::$controller;
        } else {
            $pathInfoArr = self::pathInfoToArray();
            if(!isset($pathInfoArr[1])){
                return self::$controller = I::getConfig('Controller');
            }else{
                $controller = ucwords($pathInfoArr[1]);
                //action 不写的时候默认为配置文件的默认action
                return self::$controller = $controller ;
            }
        }
    }

    /**
     * 解析url获取action名称
     * @return null|string
     */
    public static function getAction()
    {
        //静态变量如果存在就不再解析一次，节省资源
        if (null !== self::$action) {
            return self::$action;
        } else {
            $pathInfoArr = self::pathInfoToArray();
            if(isset($pathInfoArr[2])){
                $action = ucwords($pathInfoArr[2]);
                return self::$action = $action ;
            }
            //action 不写的时候默认为配置文件的默认action
            else{
                return self::$action = I::getConfig('Action');
            }
        }
    }

    /**
     * 解析PATH_INFO为数组形式
     * @return array
     */
    private static function pathInfoToArray()
    {
        if (isset($_SERVER['PATH_INFO']) && $_SERVER['PATH_INFO'] != '/') {
            return explode('/', $_SERVER['PATH_INFO']);
        } else {
            //读取配置，默认的controller和action
            return array(
                '', //模拟PATH_INFO的数组，第一个为空
                I::getConfig('Controller'),
                I::getConfig('Action')
            );
        }
    }

    /**
     * 获取url传的参数
     * 考虑到post传参，post传参是没有顺序的，因为post会把参数名字写上。
     * 问题描述：参数有几种传递方式
     * 1.想把用/分割的url按照对应的action参数传进去
     * 2.想post的参数也能传进去直接用，而不是需要手动从$_POST里面取
     * 解决方式：
     * 优先级：post > '/'分隔的参数 > get
     * todo PS:不知道REQUEST_METHOD还有没有别的方式，除了get和post
     * @return array
     */
    public static function getParams()
    {
        //优先使用POST类型的参数
        if ('POST' == $_SERVER['REQUEST_METHOD']) {
            //返回对应action参数顺序的数组（根据反射来获取action参数名称及顺序）
            $refMethod = new \ReflectionMethod(self::getNamespaceController(), self::getAction());
            $refParams = $refMethod->getParameters();
            $params = array();
            foreach ($refParams as $param) {
                $params[$param->name] = $_POST[$param->name];
            }
            return $params;
        }
        if ('GET' == $_SERVER['REQUEST_METHOD']) {
            return array_slice(self::pathInfoToArray(), 3);
        }
        throw new \Exception($_SERVER['REQUEST_METHOD'] . ' 这种请求方式 是神马？！！');
    }

}
