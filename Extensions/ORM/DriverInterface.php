<?php
/**
 *
 *
 */
namespace ORM;
use ORM\SyntaxTree;

interface DriverInterface
{
    function __construct(SyntaxTree\SyntaxTree $tree);

    function getSqlString();

    function getSqlParams(array $values);
}