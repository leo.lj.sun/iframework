<?php
/**
 * 操作类型枚举
 */
namespace ORM;
final class OperateTypeEnum
{

    const SELECT = 'SELECT';
    const DELETE = 'DELETE';
    const UPDATE = 'UPDATE';
    const INSERT = 'INSERT';
}