<?php
/**
 * User: 立杰
 * Date: 13-8-7
 * Time: 下午6:13
 */
namespace ORM\Driver\DB;
use PDO;
use Exception;

final class MyPdo
{
    private static $connection = null;

    private static $stmt = null;

    /**
     * 单例模式，声明私有，防止直接实例此类
     */
    private function __construct()
    {
    }

    /**
     * 预执行sql
     */
    public static function prepare($dbInfo, $sql)
    {
        $Link = self::initConnect($dbInfo);
        $stmt = $Link->prepare($sql);
        return $stmt;
    }

    /**
     * update和delete操作用这个
     * @param $dbInfo
     * @param $sql
     * @param $params
     * @return bool
     */
    public static function exec($dbInfo, $sql, $params)
    {
        $stmt = self::prepare($dbInfo, $sql);
        $result = $stmt->execute($params);
        self::$stmt = $stmt; //保存此对象，方便之后调用，比如取得响应行数
        return $result;

    }

    public static function rowCount()
    {
        if (null !== self::$stmt) {
            return self::$stmt->rowCount();
        } else {
            return null;
        }

    }

    private static function initConnect($dbInfo)
    {
        if (!self::$connection) {
            //todo 这个try catch 没效果
            try {
//                self::$connection =  new PDO($dbInfo['db_dns'], base64_decode($dbInfo['db_username']), base64_decode($dbInfo['db_passwd']),array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8';"));
                self::$connection = new PDO($dbInfo['db_dns'], $dbInfo['db_username'], $dbInfo['db_passwd'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8';"));

            } catch (PDOException $e) {
                print 'Connection failed: ' . $e->getMessage();
                exit;
            }
            return self::$connection;
        } else {
            return self::$connection;
        }
    }

    public static function fetchAll($dbInfo, $sql, $params)
    {
        $stmt = self::prepare($dbInfo, $sql);
        $stmt->execute($params);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function fetchRow($dbInfo, $sql, $params)
    {
        $stmt = self::prepare($dbInfo, $sql);
        $stmt->execute($params);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public static function getInsertId()
    {
        return self::$connection->lastInsertId();
    }

    public static function beginTransaction($dbInfo)
    {
        $Link = self::initConnect($dbInfo);
        $Link->beginTransaction();
    }

    public static function commit()
    {
        self::$connection->commit();
    }

    public static function rollBack()
    {
        self::$connection->rollBack();
    }

    public static function query($dbInfo, $sql, $args)
    {
        $response = self::prepare($dbInfo, $sql);
        $result = $response->execute($args);
        if(!$result) return false;
        //todo 这里需要判断是查询才返回fetchall结果
        return $response->fetchAll(PDO::FETCH_ASSOC);
    }

}
