<?php
/**
 * User: 立杰
 * Date: 13-8-7
 * Time: 下午6:04
 * 主要是拼装sql
 */
namespace ORM\Driver\DB;
use ORM;
use ORM\SyntaxTree;
use Exception;

final class MysqlDriver implements ORM\DriverInterface
{

    private $sql = null;

    private $tree = null;

    public function __construct(SyntaxTree\SyntaxTree $tree)
    {
        $this->tree = $tree;
        //过滤where 等参数
        // $this->tree->params = $this->quote($this->tree->params);
    }

    public function getSqlString()
    {
        switch ($this->tree->operateType) {
            case ORM\OperateTypeEnum::SELECT:
                return $this->getSelectSql();
                break;
            case ORM\OperateTypeEnum::INSERT:
                return $this->getInsertSql();
                break;
            case ORM\OperateTypeEnum::UPDATE:
                return $this->getUpdateSql();
                break;
            case ORM\OperateTypeEnum::DELETE:
                return $this->getDeleteSql();
                break;
        }
    }

    public function getSqlParams(array $values)
    {
        $patten = '/:(\w+)/im';
        preg_match_all($patten, $this->sql, $matchResult);
        $params = array();
        for ($i = 0; $i < count($matchResult[0]); $i++) {
            $params[$matchResult[0][$i]] = $values[$matchResult[1][$i]];
        }
        return $params;
    }

    private function getSelectSql()
    {
        if (array() == $this->tree->fields) {
            throw new Exception("fields can not be empty,plase use method 'columns' ^_^");
        }
        $this->setAlias();
        $this->sql = 'select ' . join(',', $this->tree->fields) . ' from ' . $this->tree->source . ' ';
        $this->sql .= $this->getJoin();
        $this->sql .= $this->getWhere();
        $this->sql .= $this->getIn();
        $this->sql .= $this->getNotIn();
        $this->sql .= $this->getOrders();
        $this->sql .= $this->getLimit();
        $this->getExists();
        $this->sql .= ';';
        return $this->sql;
    }

    /**
     * 查询指定数据是否存在
     */
    private function getExists()
    {
        if ($this->tree->isExists) {
            $this->sql = 'select exists(' . $this->sql . ') as result';
        }
    }

    private function getWhere()
    {
        return (array() != $this->tree->where) ? ' where ' . join(' ', $this->tree->where) : '';
    }

    private function getOrders()
    {
        return (array() != $this->tree->orderBy) ? ' order By ' . join(',', $this->tree->orderBy) : '';
    }

    private function getLimit()
    {
        return (array() != $this->tree->limit) ? ' limit ' . join(',', $this->tree->limit) : '';
    }

    private function getIn()
    {
        if ('' != $this->tree->inField && array() != $this->tree->in) {
            $in = ('' == $this->getWhere()) ? ' where ' : ' and ';
            $in .= $this->tree->inField . ' in (' . join(',', $this->tree->in) . ') ';
            return $in;
        }
    }

    private function getNotIn()
    {
        if ('' != $this->tree->notInField && array() != $this->tree->notIn) {
            $notIn = ('' == $this->getWhere()) ? ' where ' : ' and ';
            $notIn .= $this->tree->notInField . ' not in (' . join(',', $this->tree->notIn) . ') ';
            return $notIn;
        }
    }

    private function getJoin()
    {
        return (array() != $this->tree->join) ? rtrim(join(' ', $this->tree->join), ',') : '';
    }

    private function getInsertSql()
    {
        $this->sql = 'insert into ' . $this->tree->source . ' ( ' . join(',', $this->tree->fields) . ' ) values ( ' . join(',', array_map(function ($item) {
            return ':' . $item;
        }, $this->tree->fields)) . ');';
        return $this->sql;
    }

    private function getUpdateSql()
    {
        $where = $this->getWhere();
        if ($where == '') {
            throw new Exception('update parmas cannot be empty');
        }
        $this->sql = 'update ' . $this->tree->source . ' set ' . join(',', array_map(function ($item) {
            return $item . '=:' . $item;
        }, $this->tree->fields));
        $this->sql .= $where;
        return $this->sql;
    }

    private function getDeleteSql()
    {
        $where = $this->getWhere();
        if ($where == '') {
            throw new Exception('delete parmas cannot be empty');
        }
        $this->sql = 'delete from ' . $this->tree->source;
        $this->sql .= $where;
        return $this->sql;
    }

    /**
     * 过滤
     * @param $value
     * @return array|string
     */
    private function quote($value)
    {
        if (is_array($value)) {
            foreach ($value as $key => $string) {
                $value[$key] = $this->quote($string);
            }
        } else {
            //当参数为字符串或字符时
            if (is_string($value)) {
                $value = mysql_real_escape_string($value);
            }
        }
        return $value;
    }

    /*
     * join的时候为字段设置别名，方便后期处理数据
     */
    private function setAlias()
    {
        if ($this->tree->isJoin) {
            foreach ($this->tree->fields as $key => $value) {
                $result[$key] = $value . ' as ' . str_replace('.', '_', $value);
            }
            $this->tree->fields = $result;
        }
    }

}