<?php
/**
 * User: 立杰
 * Date: 13-8-7
 * Time: 下午6:25
 *
 */
namespace ORM\SyntaxTree;
use Exception;
use ORM\SyntaxTree\SyntaxTree as tree;
final class SqlSyntaxTree extends tree
{
    private $source = ''; //数据源

    private $operateType = ''; //操作类型

    private $fields = array(); //字段

    private $where = array(); //参数

    private $limit = array();

    private $orderBy = array();

    private $join = array();

    private $joinType = array(
        'innerJoin' => 'inner Join',
        'leftJoin' => 'left Join',
        'rightJoin' => 'right Join',
    );

    private $in = array();

    private $inField = '';

    private $notIn = array();

    private $notInField = '';

    private $isExists = false; //判断数据是否存在

    private $isJoin = false; //判断是否使用了join，在处理结果集的时候用到

    private $whereType = array(
        'and' => 'and',
        'or' => 'or'
    );

//    public function addParams($name,$value){
//		$this->params[$name]= $value;
//		return $this;
//    }

    public function __set($name, $value)
    {
        if (isset($this->$name)) {
            $this->$name = $value;
            return $this;
        } else {
            throw new Exception("{$name} is not exists");
        }
    }

    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        } else {
            throw new Exception("{$name} is not exists");
        }
    }

    public function setJoin($str)
    {
        array_push($this->join, $str);
    }

    public function setWhere($str)
    {
        array_push($this->where, $str);
    }

    public function setOrderBy($str)
    {
        array_push($this->orderBy, $str);
    }

    public function desc()
    {
        array_push($this->orderBy, array_pop($this->orderBy) . ' DESC ');
    }

}
