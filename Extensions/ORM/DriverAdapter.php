<?php
/**
 * User: 立杰
 * Date: 13-8-7
 * Time: 下午6:09
 */
namespace ORM;
use ORM\Driver;
use ORM\SyntaxTree;
use Exception;

final class DriverAdapter
{
    private function __construct()
    {

    }

    /*
     * 获取配置文件
     * 返回数据库实例
     */
    public static function getInstance($conf, SyntaxTree\SyntaxTree $tree)
    {
        if (is_int(strpos($conf['db_dns'], 'mysql'))) {
            return new Driver\DB\MysqlDriver($tree);
        }
    }


}
